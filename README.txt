﻿Upstream kernels on Chromebooks
You may want to run a stock upstream kernel on a Chromebook running ChromeOS for a variety of reasons:
* Testing and development of upstream patches
* Pushing upstream patches
* Doing upstream development
* Doing distro development on a Chromebook
To that end, I think it’s nice to be able to work in a regular, upstream git kernel tree and be able to easily push your kernel over to a target device for development and testing.  Further, using the stock Chromebook firmware and boot method (vboot) is desirable for reasons of test coverage, consistency of experience, and general Chromeyness.


The high level requirements for this are similar to many other development activities:
* Chromebook in developer mode
* ssh access to the target device
* Workstation with kernel sources
* vboot-kernel-utils on the workstation


On a high level, the steps required are the following:
1. Build your kernel with an appropriate config and necessary patches (see appendix)
2. Copy modules to the target device
3. Build the new kernel image, including command line, signing, and boot stub
4. Install the new image
5. Set the new image to boot once


The script in the appendix will perform these steps for you and is provided as a reference.
________________
Appendix A - command line and config


A sample command line is included in the repo:


loglevel=7
init=/sbin/init
root=/dev/nvme0n1p3
rootwait
rw
i915.modeset=1
	

It will depend on your specific platform (some will have root on /dev/mmc* for example, or not use the i915 graphics driver.


The kernel configuration to use will depend on the platform as well, but options like CONFIG_VT and FRAMEBUFFER should be disabled if booting Chrome OS. See this example config for reference.


See example hatch config in this dir.


On Chrome OS, the current config can also be retrieved using “modprobe configs” and then reading /proc/config.gz.
________________
Appendix B - Scripts
git://people.freedesktop.org/~jbarnes/cros-misc


Use the push_kernel.sh script to upload a kernel from your current working directory (which should be a kernel tree) to the target system.  The target has to be in developer mode with ssh access, and you’ll also need to provide a kernel boot command line.  You can base your command line on /proc/cmdline from the running system.
